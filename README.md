
## Method: concat

1. **Parameter it accepts:**  
n (any) number of values (number, string, boolean, array, null, undefined, object and function etc).

2. **what it will return:**  
a single Array consisting of by all the values passed as parameters in the same order.

3. **Example**

       let numbers = [1, 2, 3];

       numbers.concat(4); //[1,2,3,4]

        let sentanceArray = 'A quick brown fox jumped over a lazy'.split(' ');

       sentanceArray.concat('dog').join(' '); //"A quick brown fox jumped
       over a lazy dog"
       let colors = ['red', 'green', 'blue'];
       colors.concat('black', 'red', 21, true); // ['red','green','blue','black', 'red', 21, true]

4. **Explanation:** 

   concat accepts n number of values and returns one array with all the values in same order. It does not change the original array.

5. **Does it mutate the original array?:**  No it does not mutate the original array.


## Method: flat

1. **Parameter it accepts:**  Array

2. **what it will return:**  
A new array with the sub-array elements concatenated into it.

3. **Example**

       const arr1 = [0, 1, 2, [3, 4]];
       console.log(arr1.flat());
       // expected output: [0, 1, 2, 3, 4]

       const arr2 = [0, 1, 2, [[[3, 4]]]];

       console.log(arr2.flat(2));
       // expected output: [0, 1, 2, [3, 4]]

4. **Explanation:** 
The flat() method creates a new array with all sub-array elements concatenated into it recursively up to the specified depth.

5. **Does it mutate the original array?:**   
No it does not mutate the original array.


## Method: push

1. **Parameter it accepts:**  
The element(s) to add to the end of the array.

2. **what it will return:**  
The new length property of the object upon which the method was called.

3. **Example**

       const animals = ['pigs', 'goats', 'sheep'];

       const count = animals.push('cows');
       console.log(count);
       // expected output: 4
       console.log(animals);
       // expected output: Array ["pigs", "goats", "sheep", "cows"]

       animals.push('chickens', 'cats', 'dogs');
       console.log(animals);
       // expected output: Array ["pigs", "goats", "sheep", "cows", "chickens", "cats", "dogs"]


4. **Explanation:**

    The push() method appends values to an array.

5. **Does it mutate the original array?:** Yes


## Method: pop

1. **Parameter it accepts:**
Array

2. **what it will return:**  
The removed element from the array; undefined if the array is empty.

3. **Example**
        
       const plants = ['broccoli', 'cauliflower', 'cabbage', 'kale', 'tomato'];

       console.log(plants.pop());
       // expected output: "tomato"

       console.log(plants);
       // expected output: Array ["broccoli", "cauliflower", "cabbage", "kale"]

       plants.pop();

       console.log(plants);
       // expected output: Array ["broccoli", "cauliflower", "cabbage"]

4. **Explanation:** 

   The pop() method removes the last element from an array and returns that element. This method changes the length of the array.

5. **Does it mutate the original array?:** Yes


## Method: shift

1. **Parameter it accepts:**
Array

2. **what it will return:**  
The removed element from the array; undefined if the array is empty.

3. **Example**

       const array1 = [1, 2, 3];

       const firstElement = array1.shift();

       console.log(array1);
       // expected output: Array [2, 3]

       console.log(firstElement);
       // expected output: 1

4. **Explanation:** 

   The shift() method removes the first element from an array and returns that removed element. This method changes the length of the array.

5. **Does it mutate the original array?:** Yes



## Method: unshift

1. **Parameter it accepts:**
Array

2. **what it will return:**  
The new length property of the object upon which the method was called.

3. **Example**
 
       const array1 = [1, 2, 3];

       console.log(array1.unshift(4, 5));
       // expected output: 5

       console.log(array1);
       // expected output: Array [4, 5, 1, 2, 3]


4. **Explanation:** 

    The unshift() method adds one or more elements to the beginning of an array and returns the new length of the array.

5. **Does it mutate the original array?:** Yes


## Method: indexOf

1. **Parameter it accepts:** string, Array

2. **what it will return:**  
   Element to locate in the array.

3. **Example**

       const beasts = ['ant', 'bison', 'camel', 'duck', 'bison'];

       console.log(beasts.indexOf('bison'));
       // expected output: 1

       // start from index 2
       console.log(beasts.indexOf('bison', 2));
       // expected output: 4

       console.log(beasts.indexOf('giraffe'));
       // expected output: -1

4. **Explanation:** 

   The indexOf() method returns the first index at which a given element can be found in the array, or -1 if it is not present.

5. **Does it mutate the original array?:** No



## Method: lastIndexOf

1. **Parameter it accepts:** stringToSearch and position 

2. **what it will return:** 

   The last index of the element in the array; -1 if not found.

3. **Example**

       const animals = ['Dodo', 'Tiger', 'Penguin', 'Dodo'];

       console.log(animals.lastIndexOf('Dodo'));
       // expected output: 3

       console.log(animals.lastIndexOf('Tiger'));
       // expected output: 1


4. **Explanation:** 

   The lastIndexOf() method returns the last index at which a given element can be found in the array, or -1 if it is not present. The array is searched backwards, starting at fromIndex.



5. **Does it mutate the original array?:** No



## Method: includes

1. **Parameter it accepts:** Array, string

2. **what it will return:**  
A boolean value which is true if the value searchElement is found within the array (or the part of the array indicated by the index fromIndex, if specified).


3. **Example**

       const array1 = [1, 2, 3];

       console.log(array1.includes(2));
       // expected output: true

       const pets = ['cat', 'dog', 'bat'];

       console.log(pets.includes('cat'));
       // expected output: true

       console.log(pets.includes('at'));
       // expected output: false


4. **Explanation:** 

   The includes() method determines whether an array includes a certain value among its entries, returning true or false as appropriate.

5. **Does it mutate the original array?:** No


## Method: reverse

1. **Parameter it accepts:** Array elements

2. **what it will return:**  
The reference to the original array, now reversed. Note that the array is reversed in place, and no copy is made.



3. **Example**

       const items = [1, 2, 3];
       console.log(items); // [1, 2, 3]

       items.reverse();
       console.log(items); // [3, 2, 1]

4. **Explanation:** 

   The reverse() method transposes the elements of the calling array object in place, mutating the array, and returning a reference to the array.



5. **Does it mutate the original array?:** Yes


## Method: splice

1. **Parameter it accepts:** Array elements

2. **what it will return:**  
An array containing the deleted elements.

   If only one element is removed, an array of one element is returned.

   If no elements are removed, an empty array is returned.


3. **Example**

       const months = ['Jan', 'March', 'April', 'June'];
       months.splice(1, 0, 'Feb');
       // inserts at index 1
       console.log(months);
       // expected output: Array ["Jan", "Feb", "March", "April", "June"]

       months.splice(4, 1, 'May');
       // replaces 1 element at index 4
       console.log(months);
       // expected output: Array ["Jan", "Feb", "March", "April", "May"]


4. **Explanation:** 

    The splice() method changes the contents of an array by removing or replacing existing elements and/or adding new elements in place. To access part of an array without modifying it, see slice().


5. **Does it mutate the original array?:** Yes


## Method: slice

1. **Parameter it accepts:** Array elements

2. **what it will return:**  
A new array containing the extracted elements.

3. **Example**

       const fruits = ['Banana', 'Orange', 'Lemon', 'Apple', 'Mango'];
       const citrus = fruits.slice(1, 3);

       // fruits contains ['Banana', 'Orange', 'Lemon', 'Apple', 'Mango']
       // citrus contains ['Orange','Lemon']

4. **Explanation:** 

   The slice() method returns a shallow copy of a portion of an array into a new array object selected from start to end (end not included) where start and end represent the index of items in that array. The original array will not be modified.

5. **Does it mutate the original array?:** No


## Method: forEach

1. **Parameter it accepts:** Array, callbackFn, element, index, thisArg

2. **what it will return:**  undefined

3. **Example**

       const array1 = ['a', 'b', 'c'];

       array1.forEach(element => console.log(element));

       // expected output: "a"
       // expected output: "b"
       // expected output: "c"


4. **Explanation:** 

   The forEach() method executes a provided function once for each array element.


5. **Does it mutate the original array?:** No


## Method: map

1. **Parameter it accepts:** Array, callbackFn, element, index, thisArg

2. **what it will return:** 

   A new array with each element being the result of the callback function.

3. **Example**

        const array1 = [1, 4, 9, 16];

       // pass a function to map
       const map1 = array1.map(x => x * 2);

       console.log(map1);
       // expected output: Array [2, 8, 18, 32]


4. **Explanation:** 

    The map() method creates a new array populated with the results of calling a provided function on every element in the calling array.


5. **Does it mutate the original array?:** No


## Method: filter

1. **Parameter it accepts:** Array, callbackFn, element, index, thisArg

2. **what it will return:** 

   A shallow copy of a portion of the given array, filtered down to just the elements from the given array that pass the test implemented by the provided function. If no elements pass the test, an empty array will be returned.



3. **Example**

       const words = ['spray', 'limit', 'elite', 'exuberant', 'destruction', 'present'];

       const result = words.filter(word => word.length > 6);

       console.log(result);
       // expected output: Array ["exuberant", "destruction", "present"]


4. **Explanation:** 

   The filter() method creates a shallow copy of a portion of a given array, filtered down to just the elements from the given array that pass the test implemented by the provided function.

5. **Does it mutate the original array?:** No


## Method: find

1. **Parameter it accepts:** Array, callbackFn, element, index, thisArg

2. **what it will return:** 

   The first element in the array that satisfies the provided testing function. Otherwise, undefined is returned.



3. **Example**

       const array1 = [5, 12, 8, 130, 44];

       const found = array1.find(element => element > 10);

       console.log(found);
       // expected output: 12


4. **Explanation:**

   The find() method returns the first element in the provided array that satisfies the provided testing function. If no values satisfy the testing function, undefined is returned.


5. **Does it mutate the original array?:** No


## Method: findIndex

1. **Parameter it accepts:** Array, callbackFn, element, index, thisArg

2. **what it will return:**  

   The index of the first element in the array that passes the test. Otherwise, -1.

3. **Example**

       const array1 = [5, 12, 8, 130, 44];

       const isLargeNumber = (element) => element > 13;

       console.log(array1.findIndex(isLargeNumber));
       // expected output: 3


4. **Explanation:** 

   The findIndex() method returns the index of the first element in an array that satisfies the provided testing function. If no elements satisfy the testing function, -1 is returned.

5. **Does it mutate the original array?:** No


## Method: some

1. **Parameter it accepts:** Array, callbackFn, element, index, thisArg

2. **what it will return:** 

   true if the callback function returns a truthy value for at least one element in the array. Otherwise, false.

3. **Example**

       const array = [1, 2, 3, 4, 5];

       // checks whether an element is even
       const even = (element) => element % 2 === 0;

       console.log(array.some(even));
       // expected output: true


4. **Explanation:** 

   The some() method tests whether at least one element in the array passes the test implemented by the provided function. It returns true if, in the array, it finds an element for which the provided function returns true; otherwise it returns false. It doesn't modify the array.

5. **Does it mutate the original array?:** No 


## Method: every

1. **Parameter it accepts:** Array, callbackFn, element, index, thisArg

2. **what it will return:** 

   true if callbackFn returns a truthy value for every array element. Otherwise, false.

3. **Example**

       const isBelowThreshold = (currentValue) => currentValue < 40;

       const array1 = [1, 30, 39, 29, 10, 13];

       console.log(array1.every(isBelowThreshold));
       // expected output: true

4. **Explanation:**

   The every() method tests whether all elements in the array pass the test implemented by the provided function. It returns a Boolean value.

5. **Does it mutate the original array?:** No


## Method: sort

1. **Parameter it accepts:** 

   a
The first element for comparison.

   b
The second element for comparison.


2. **what it will return:** 

   The reference to the original array, now sorted. Note that the array is sorted in place, and no copy is made.

3. **Example**

       const months = ['March', 'Jan', 'Feb', 'Dec'];
       months.sort();
       console.log(months);
       // expected output: Array ["Dec", "Feb", "Jan", "March"]

       const array1 = [1, 30, 4, 21, 100000];
       array1.sort();
       console.log(array1);
       // expected output: Array [1, 100000, 21, 30, 4]


4. **Explanation:**

   The sort() method sorts the elements of an array in place and returns the reference to the same array, now sorted. The default sort order is ascending, built upon converting the elements into strings, then comparing their sequences of UTF-16 code units values.

5. **Does it mutate the original array?:** No

## Method: reduce

1. **Parameter it accepts:** Array, callbackFn, accumulator, currentValue, currentIndex.

2. **what it will return:**  

   The value that results from running the "reducer" callback function to completion over the entire array.

3. **Example**

       const array1 = [1, 2, 3, 4];

       // 0 + 1 + 2 + 3 + 4
       const initialValue = 0;
       const sumWithInitial = array1.reduce(
       (previousValue, currentValue) => previousValue + currentValue,
       initialValue
       );

       console.log(sumWithInitial);
       // expected output: 10


4. **Explanation:** 

   The reduce() method executes a user-supplied "reducer" callback function on each element of the array, in order, passing in the return value from the calculation on the preceding element. The final result of running the reducer across all elements of the array is a single value.

5. **Does it mutate the original array?:** No
